import spotipy
from spotipy.oauth2 import SpotifyOAuth

SPOTIFY_ID = "036702c6c06f40a8acf660c94b299818"
SPOTIFY_SECRET = "d1d3b07f25c44026a3470f62340f317c"
SPOTIFY_URL = 'http://example.com'


class Spotipy:
    def __init__(self):
        self.sp = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=SPOTIFY_ID,
                                                            client_secret=SPOTIFY_SECRET,
                                                            redirect_uri=SPOTIFY_URL,
                                                            scope="playlist-modify-private"))
        # self.create_playlist()

    def search(self, Q, Year=2008):
        return self.sp.search(q=f'track:{Q} year:{Year}', type='track')

    def create_playlist(self, name = 2008):
        user_id = self.sp.current_user()['id']
        self.id =  self.sp.user_playlist_create(user = user_id, name = name, public=False, collaborative=False, description='BobiKing')

    def add_song(self, songs):
        self.sp.playlist_add_items(playlist_id=self.id['id'],items=songs)