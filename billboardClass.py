import requests as r
from bs4 import BeautifulSoup


class Billboard():
    def __init__(self):
        self.request =  "https://www.billboard.com/charts/hot-100/2008-12-02"
        self.songs = self.beautiful()

    def beautiful(self):
        req = r.get(self.request).text
        songs = BeautifulSoup(req, 'html.parser')
        return songs.select("li ul li h3")  # custom
