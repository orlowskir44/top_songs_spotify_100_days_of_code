import billboardClass as b
import spotipyClass as s


def spotipy_uri(song_list, save_songs_list):
    for save_song in song_list:
        result = sp.search(save_song)
        try:
            uri = result['tracks']['items'][0]['uri']
            print(f"☑️{result['tracks']['items'][0]['name']}")
            save_songs_list.append(uri)
        except IndexError:
            print(f'🚫Song {save_song} not exist in Spotify. Skipped')


# i_date = input("Witch year you would like to travel to? [Format: YYYY-MM-DD] >>")

billboard = b.Billboard()
sp = s.Spotipy()
sp.create_playlist()

# Create List
songs_names = [s.get_text().strip() for s in billboard.beautiful()]
songs_uris = []

spotipy_uri(songs_names, songs_uris)

# SPOTIFY:
sp.add_song(songs_uris)
